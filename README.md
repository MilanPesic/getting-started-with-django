# Testing Django

## Add to etc/hosts:
127.0.0.1 api.dev.django

### Create Django project
```bash
docker-compose run api django-admin startproject config .
```

### Create new app inside Django project
```bash
docker-compose run api django-admin startapp test_api_1
```

### Change ownership of new files created by django-admin
```bash
sudo chown -R $USER:$USER api api/manage.py
``` 

### Create new migration
```bash
docker-compose exec api python manage.py makemigrations --name create_superuser test_api_1 --empty
```

### Run project
```bash
docker-compose up --build --remove-orphans
```
