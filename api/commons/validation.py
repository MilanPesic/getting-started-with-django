from commons.validators import FileValidator, LengthValidator


# VALIDATE UPLOADED FILES
validate_file = FileValidator(
        allowed_extensions = [
            'jpg', 
            'jpeg', 
            'png', 
            'pdf'
        ],
        max_size = 15024,
        content_type = [
            'image/jpg',
            'image/jpeg',
            'image/png', 
            'application/pdf'
        ]
    )


# VALIDATE PASSWORD LENGTH
validate_password_length = LengthValidator(
    min_length = 8, 
    max_length = 16,
    field_name = 'password'
)


# VALIDATE USERNAME LENGTH
validate_username_length = LengthValidator(
    min_length = 3, 
    max_length = 50,
    field_name = 'username'
)
