import json

from django.core import serializers
from django.contrib.auth.tokens import default_token_generator


def serialize(object):
    """
    Serialize model object and return dict using custom helper.
    """
    obj = serializers.serialize('json', [object])
    obj = json.loads(obj)
    obj = obj[0]['fields']

    return obj


def make_token(user):
    """
    Generate one time token using custom helper.
    """
    token = default_token_generator.make_token(user)
    
    return token


def check_token(user, token):
    """
    Check generated one time token using custom helper.
    """
    token = default_token_generator.check_token(user, token)
    
    return token
    