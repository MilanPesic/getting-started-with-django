from django.core.mail import send_mail, EmailMessage, EmailMultiAlternatives


def send_email(
    subject: str,
    message: str,
    from_email: str,
    recipient_list: list,
    html_message: str = None,
    attach_file: str = None
):

    try:
        
        # Send both text and HTML versions of a message
        mail = EmailMultiAlternatives(
            subject, message, from_email, recipient_list
        )

        # Enables sending mail with text/html content type
        if html_message:
            mail.attach_alternative(html_message, "text/html")

        # Creates a new attachment using a file from your filesystem
        if attach_file:
            mail.attach_file(attach_file)

        # Send email by calling send() method
        mail = mail.send()
        
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'Email message was not send due to error.'
        }

    return {
        'status': 'success',
        'message': 'Email was sent successfully.',
        'mail': mail
    }
    