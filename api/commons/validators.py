from django.utils.deconstruct import deconstructible
from django.core.exceptions import ValidationError
from pathlib import Path
from django.template.defaultfilters import filesizeformat


@deconstructible
class LengthValidator:

    message = {
        'min_length': ("Minumum length of %(field_name)s must be at least %(min_length)s characters."),
        'max_length': ("Maximum length of %(field_name)s must be %(max_length)s characters.")
    }

    def __init__(self, min_length: int = None, max_length: int = None, field_name: str = None):
        self.min_length = min_length
        self.max_length = max_length
        self.field_name = field_name

    def __call__(self, value):
        if self.min_length and len(value) < self.min_length:
            raise ValidationError(
                self.message['min_length'],
                params = {
                    'min_length': self.min_length,
                    'field_name': self.field_name,
                    'value': value,
                },
            )
        
        if self.max_length and len(value) > self.max_length:
            raise ValidationError(
                self.message['max_length'],
                params = {
                    'max_length': self.max_length,
                    'field_name': self.field_name,
                    'value': value,
                },
            )

    def __eq__(self, other):
        return (
            isinstance(other, self.__class__)
            and self.min_length == other.min_length
            and self.max_length == other.max_length
            and self.field_name == other.field_name
            and self.message == other.message
        )


@deconstructible
class FileValidator:
    message = {
            'allowed_extensions': (
                "File extension “%(extension)s” is not allowed." 
                " Allowed extensions are: %(allowed_extensions)s."
            ),
            'max_size': (
                "Max file size must not be greater than %(max_size)s." 
                " Your file size is %(size)s."
            ),
            'content_types': (
                "MIME type '%(content_type)s' is not allowed." 
                " Allowed types are: %(allowed_content_types)s."
            )
        }

    def __init__(self, allowed_extensions: list = None, max_size: str = None, content_type: list = None):
        self.allowed_extensions = allowed_extensions
        self.max_size = max_size
        self.content_type = content_type
        
    def __call__(self, value):
        extension = Path(value.name).suffix[1:].lower()
        if self.allowed_extensions and extension not in self.allowed_extensions:
            raise ValidationError(
                self.message['allowed_extensions'],
                params = {
                    'extension': extension,
                    'allowed_extensions': ', '.join(self.allowed_extensions),
                    'value': value,
                },
            )

        size = value.file.size
        if self.max_size and size > self.max_size:
            raise ValidationError(
                self.message['max_size'],
                params = {
                    'max_size': filesizeformat(self.max_size), 
                    'size': filesizeformat(size),
                    'value': value,
                },
            )

        content_type = value.file.content_type
        if self.content_type and content_type not in self.content_type:
            raise ValidationError(
                self.message['content_types'],
                params= {
                    'content_type': content_type,
                    'allowed_content_types': ', '.join(self.content_type),
                    'value': value,
                },
            )

    def __eq__(self, other):
        return (
            isinstance(other, self.__class__)
            and self.allowed_extensions == other.allowed_extensions
            and self.max_size == other.max_size
            and self.content_type == other.content_type
            and self.message == other.message
        )
