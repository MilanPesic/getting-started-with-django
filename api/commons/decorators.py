import json
from functools import wraps
from django.core.exceptions import ValidationError


def required_fields(*required):
    def decorator(func):
        @wraps(func)
        def wrapped_data(request, *args, **kwargs):

            data = json.loads(request.body)

            missing_fields: list = []

            # Check if all required fields are present in GET and POST request
            try:
                for x in kwargs:
                    if x not in required or not kwargs[x]:
                        missing_fields.append(x)

                for x in data:
                    if x not in required or not data[x]:
                        missing_fields.append(x)

                if missing_fields:
                    raise ValidationError(f"Missing required fields: {', '.join(missing_fields)}")

            except ValidationError as e:
                return {
                    'status': 'error',
                    'status_code': 409,
                    'data': {},
                    'error': f'{e!r}',
                    'message': e.message
                }

            return func(request, *args, **kwargs)
        return wrapped_data
    return decorator


def required_form_data_fields(*required):
    def decorator(func):
        @wraps(func)
        def wrapped_data(request, *args, **kwargs):

            data = request.POST

            for i in data:
                data = json.loads(data[i])

            missing_fields: list = []

            # Check if all required fields are present in GET and POST request
            try:
                for x in kwargs:
                    if x not in required or not kwargs[x]:
                        missing_fields.append(x)

                for x in data:
                    if x not in required or not data[x]:
                        missing_fields.append(x)

                if missing_fields:
                    raise ValidationError(f"Missing required fields: {', '.join(missing_fields)}")

            except ValidationError as e:
                return {
                    'status': 'error',
                    'status_code': 409,
                    'data': {},
                    'error': f'{e!r}',
                    'message': e.message
                }

            return func(request, *args, **kwargs)
        return wrapped_data
    return decorator

