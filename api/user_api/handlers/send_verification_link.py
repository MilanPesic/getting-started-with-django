import json

from django.contrib.auth.hashers import check_password
from django.core.exceptions import ValidationError
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt
from django.core.validators import validate_email
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.template.loader import render_to_string

from commons.decorators import required_fields
from commons.helpers import serialize, make_token
from commons.mailer import send_email
from user_api.models.user_management import UserManagement


@require_POST
@csrf_exempt
@required_fields('email')
def send_verification_link(request):
    """
    Exposed on route: 'api/1.0/users/resend-link'
    This route resend verify email link to the user
    Expected payload:
        email,
        attach_file[optional]
    """
    data = json.loads(request.body)

    email = data['email']
    attach_file = data.get('attach_file')

    # VALIDATE EMAIL
    try:
        email_validation = validate_email(email)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': e.message
        }
    
    # FIND USER BY EMAIL
    try:
        user = UserManagement.objects.get(email=email)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'User not found, try different email address.'
        }

    # CREATE VERIFICATION LINK FOR THE USER
    try:
        if user.is_verified:
            return {
                'status': 'success',
                'status_code': 200,
                'data': {},
                'error': None,
                'message': 'User is already verified.'
            }

        token = make_token(user)
        id = urlsafe_base64_encode(force_bytes(user.id))
        verification_link = f'https://api.dev.django/api/1.0/users/verify-user-account/{id}/{token}'
        
        # Create user dict with returned object values
        user_info: dict = {
            'first_name': user.first_name,
            'last_name': user.last_name,
            'verification_link': verification_link
        }

    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': str(e)
        }
    
    # SEND LINK TO THE USER
    subject = 'Account Verification'
    from_email = 'admin@django.com'
    recipient_list = [email]

    context: dict = {
        'first_name': user.first_name,
        'verification_link': verification_link
    }

    message = render_to_string('email/account-verification.txt', context)
    html_message = render_to_string('email/account-verification.html', context)

    mail = send_email(
        subject,
        message,
        from_email,
        recipient_list,
        html_message,
        attach_file
    )

    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'user_info': user_info,
            'mail': mail
        },
        'error': None,
        'message': 'Ok',
    }
