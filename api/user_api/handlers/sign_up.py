import json

from django.contrib.auth.hashers import make_password
from django.core.exceptions import ValidationError
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt

from commons.decorators import required_fields
from commons.helpers import serialize
from user_api.models.user_management import UserManagement


@required_fields('username', 
        'email', 
        'password', 
        'phone', 
        'first_name', 
        'last_name', 
        'description')
@csrf_exempt
@require_POST
def sign_up(request):
    """
    Exposed on route: 'api/1.0/users/sign-up'
    This route creates new user
    Expected payload:
        username,
        email,
        password,
        phone,
        first_name,
        last_name,
        description
    """
    data = json.loads(request.body)

    # HASH PASSWORD USING DJANGO'S DEFAULT HASHER
    password = data['password']
    password = make_password(password)
    
    # USER MANAGEMENT
    try:
        # Create new UserManagement model instance
        user = UserManagement(**data)

        # Validate data before inserting into database
        user.full_clean()

        # Update password after hashing
        user.password = password

        # Save data into database
        user.save()

        # Get currently saved object from database
        user = serialize(user)
        
        # Create user dict with returned object values
        user: dict = {
            'first_name': user['first_name'],
            'last_name': user['last_name']
        }
    except ValidationError as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': e.message_dict
        }
    
    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'user': user
        },
        'error': None,
        'message': 'Ok',
    }
