import json

from django.http import FileResponse, HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET, require_POST
from django.contrib.postgres import search

from user_api.models import UserManagement, user_management
from commons.helpers import serialize


@require_POST
@csrf_exempt
def standard_textual_search(request):
    """
    Exposed on route: '/api/1.0/users/standard-textual-search'
    This route allows searching of user model
    Expected payload:
        search_term
    """
    # Convert JSON request to dict
    data = json.loads(request.body)

    # Get search term for searching database
    search_term = data['search_term']

    # Set empty list of search results so you can append results
    search_results: list = []

    # Get search results from database
    try:
        results = UserManagement.objects.filter(first_name__icontains=search_term)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': str(e)
        }

    # Loop through results and return queryset object
    for result in results:

        # Serialize queryset object and return dict
        result = serialize(result)
        del result['password']

        # Append result dict to search results list
        search_results.append(result)

    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'search_results': search_results
        },
        'error': None,
        'message': 'Ok',
    }


@require_POST
@csrf_exempt
def full_text_search(request):
    """
    Exposed on route: '/api/1.0/users/full-text-search'
    This route allows searching of user model
    Expected payload:
        search_term
    """
    # Convert JSON request to dict
    data = json.loads(request.body)

    # Get search term for searching database
    search_term = data['search_term']

    # Set empty lists so you can append search results
    search_lookup_results: list = []
    search_vector_results: list = []
    search_query_results: list = []
    search_rank_results: list = []
    search_headline_result: list = []

    # Get search results using search lookup
    try:
        search_lookup = UserManagement.objects.filter(first_name__search=search_term)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': str(e)
        }

    # Get search results using SearchVector class
    try:
        search_vector = UserManagement.objects.annotate(
            search=search.SearchVector('first_name', 'last_name', 'description'))
        search_vector = search_vector.filter(search=search_term)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': str(e)
        }

    # Get search results using SearchVector and SearchQuery class
    try:
        vector = search.SearchVector('first_name', 'last_name', 'description')
        query = search.SearchQuery(search_term, search_type="websearch")
        search_query = UserManagement.objects.annotate(search=vector).filter(search=query)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': str(e)
        }

    # Get search results using SearchRank class
    try:
        vector = search.SearchVector('first_name', 'last_name', 'description', config='english')
        query = search.SearchQuery(search_term, search_type="websearch")
        rank = search.SearchRank(vector, query)
        search_rank = UserManagement.objects.annotate(search=vector, rank=rank).filter(search=query).order_by('-rank')
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': str(e)
        }

    # Get search results using SearchHeadline class
    try:
        vector = search.SearchVector('first_name', 'last_name', 'description', config='english')
        query = search.SearchQuery(search_term, search_type="websearch")
        rank = search.SearchRank(vector, query)
        headline = search.SearchHeadline('description', query, start_sel='<span>', stop_sel='</span>')
        search_headline = UserManagement.objects.annotate(
            search=vector, 
            headline=headline, 
            rank=rank
        ).filter(search=query).order_by('-rank')
    
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': str(e)
        }

    # Loop through results and return queryset object
    for result in search_lookup:

        # Serialize queryset object and return dict
        result = serialize(result)
        del result['password']

        # Append result dict to search lookup results list
        search_lookup_results.append(result)

    # Loop through results and return queryset object
    for result in search_vector:

        # Serialize queryset object and return dict
        result = serialize(result)
        del result['password']

        # Append result dict to search vector results list
        search_vector_results.append(result)

    # Loop through results and return queryset object
    for result in search_query:

        # Serialize queryset object and return dict
        result = serialize(result)
        del result['password']

        # Append result dict to search query results list
        search_query_results.append(result)

    # Loop through results and return queryset object
    for result in search_rank:

        # Serialize queryset object and return dict
        result = serialize(result)
        del result['password']

        # Append result dict to search rank results list
        search_rank_results.append(result)

    # Loop through results and return queryset object
    for result in search_headline:

        # Append result dict to search headline results list
        search_headline_result.append({
            'headline': result.headline
        })

    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'search_lookup_results': search_lookup_results,
            'search_vector_results': search_vector_results,
            'search_query_results': search_query_results,
            'search_rank_results': search_rank_results,
            'search_headline_result': search_headline_result
        },
        'error': None,
        'message': 'Ok',
    }

# Trigram similarity
"""
City.objects.filter(name__trigram_similar="Middlesborough")
['<City: Middlesbrough>']
"""

# Unaccent
"""
City.objects.filter(name__unaccent="México")
['<City: Mexico>']

User.objects.filter(first_name__unaccent__startswith="Jerem")
['<User: Jeremy>', '<User: Jérémy>', '<User: Jérémie>', '<User: Jeremie>']
"""
