import json

from django.views.decorators.http import require_GET, require_POST
from django.utils.http import urlsafe_base64_decode

from user_api.models.user_management import UserManagement
from commons.helpers import check_token


@require_GET
def verify_user_account(request, uidb64, token):
    """
    Exposed on route: 'api/1.0/users/verify-user-account/<uidb64>/<token>'
    This route accepts verification link for verifying user account
    Expected payload:
        uidb64,
        token
    """
    data = json.loads(request.body)

    id = urlsafe_base64_decode(uidb64)

    # GET USER FROM DATABASE BY ID
    try:
        user = UserManagement.objects.get(id=id)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'User does not exist.'
        }

    # CHECK IF USER ALREADY VERIFIED EMAIL
    if user.is_verified:
        return {
            'status': 'success',
            'status_code': 200,
            'data': {},
            'error': None,
            'message': 'User is already verified.'
        }

    # CHECK AND VERIFY TOKEN AND USER
    try:
        verify_token = check_token(user, token) 

        # If token is not verified, return error message
        if not verify_token:
            return {
                'status': 'error',
                'status_code': 409,
                'data': {},
                'error': 'Conflict',
                'message': 'Token not verified, there is a mismatch.'
            }

        # Update user account if token is verified
        user.is_verified = True

        # Save changes to database
        user.save()

    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': e.message
        }
    
    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'verified': user.is_verified
        },
        'error': None,
        'message': 'Ok',
    }
