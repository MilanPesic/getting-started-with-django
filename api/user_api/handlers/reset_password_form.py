import json

from django.views.decorators.http import require_GET, require_POST, require_http_methods
from django.utils.http import urlsafe_base64_decode
from django.core.exceptions import ValidationError
from django.views.decorators.csrf import csrf_exempt
from commons.decorators import required_fields
from django.contrib.auth.hashers import check_password, make_password
from django.shortcuts import render, redirect
from django.http.response import HttpResponse, Http404

from user_api.models.user_management import UserManagement
from commons.helpers import check_token


@require_http_methods(['GET', 'POST'])
@csrf_exempt
# @required_fields('password', 'confirm_password', 'new_password', 'uidb64', 'token')
def reset_password_form(request, uidb64, token):
    """
    Exposed on route: 'api/1.0/users/reset-password-form/<uidb64>/<token>'
    This route allows user reset password 
    Expected payload:
    [GET]
        uidb64,
        token
    [POST]
        password,
        confirm_password,
        new_password
    """
    uid = urlsafe_base64_decode(uidb64)
    errors: list = []

    # GET USER FROM DATABASE BY ID
    try:
        user = UserManagement.objects.get(id=uid)
    except Exception as e:
        return Http404

    #CHECK AND VERIFY TOKEN AND USER
   
    verify_token = check_token(user, token) 

    # If token is not verified, return error message
    if not verify_token:
        return render(request, 'password/reset-password-form.html', {'verify_token': verify_token})
    
    # CHANGE USER PASSWORD AFTER TOKEN VERIFICATION
    if request.method == 'POST':

        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        new_password = request.POST['new_password']

        # VALIDATE PASSWORD FIELD
       
        # Check if confirm password match with password
        if confirm_password != password:
            errors.append('Confirm password does not match password.')

        # Check password length
        if len(password) < 8 :
            errors.append(f'The password must be at least 8 characters')

        if len(new_password) < 8:
            errors.append(f'The new password must be at least 8 characters')
        
        if len(password) > 16:
            errors.append(f'The password must be a maximum of 16 characters')

        if len(new_password) > 16:
            errors.append(f'The new password must be a maximum of 16 characters')

        # Get encoded password if user exists
        encoded = user.password

        # Check if password match and return user
        password = check_password(password, encoded)
        
        # if password check_returns false, raise error message
        if not password:
            errors.append('The old password is wrong, please provide right password.')

        if not errors:

            # Hash new password 
            new_password = make_password(new_password)

            # Update password field with new password
            user.password = new_password

            # Save new password into database
            user.save()

            return HttpResponse('You changed password successfully.')

    context = {
        'uidb64': uidb64,
        'token': token,
        'verify_token': verify_token,
        'errors': errors
    }

    return render(request, 'password/reset-password-form.html', context)
    