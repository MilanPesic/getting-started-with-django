import json

from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ValidationError

from user_api.models import UserManagement
from commons.validators import FileValidator


@require_POST
@csrf_exempt
def upload_file(request):
    """
    Exposed on route: 'api/1.0/users/upload-file'
    This route allows user to upload files and json data 
    Expected payload:
        upload_file,
        json_data
    """
    # Set upload file to None if user does not want to upload anything
    upload_file: None = None

    # We are using request.POST because this is multipart/form-data
    data = request.POST['json_data']

    # Convert JSON string to dict
    data = json.loads(data)

    first_name: str = data['first_name']
    age: str = data['age']

    # VALIDATE FILE IF IT IS UPLOADED
    if 'upload_file' in request.FILES:
        upload_file = request.FILES['upload_file']

    # GET USER FROM DATABASE BY ID
    try:
        user = UserManagement.objects.get(id=1)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': f'User {first_name} does not exist, wrong ID provided.'
        }

    # Exclude all fields that are not going to be updated
    exclude = [field.name for field in user._meta.fields if field.name != 'upload_file']
        
    # Update upload_file field with uploaded file
    user.upload_file = upload_file
    
    # VALIDATE UPLOAD FILE FIELD AND EXCLUDE OTHER FIELDS
    try:
        user.full_clean(exclude = exclude)
    except ValidationError as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': e.message_dict
        }

    # Save updated field only using update_fields(['upload_file]) method
    user.save(update_fields=['upload_file'])
    
    # Check if file is uploaded
    if user.upload_file:
        upload_file = True

    # Return user and uploaded file from database
    user: dict = {
        'first_name': user.first_name,
        'upload_file': upload_file
    }
    
    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'user': user
        },
        'error': None,
        'message': 'Ok',
    }
