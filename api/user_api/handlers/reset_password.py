import json

from django.views.decorators.http import require_GET, require_POST, require_http_methods
from django.utils.http import urlsafe_base64_decode
from django.core.exceptions import ValidationError
from django.views.decorators.csrf import csrf_exempt
from commons.decorators import required_fields
from django.contrib.auth.hashers import check_password, make_password

from user_api.models.user_management import UserManagement
from commons.helpers import check_token


@require_http_methods(['GET', 'POST'])
@csrf_exempt
@required_fields('password', 'confirm_password', 'new_password', 'uidb64', 'token')
def reset_password(request, uidb64, token):
    """
    Exposed on route: 'api/1.0/users/reset-form/<uidb64>/<token>'
    This route allows user reset password 
    Expected payload:
    [GET]
        uidb64,
        token
    [POST]
        password,
        confirm_password,
        new_password
    """
    uid = urlsafe_base64_decode(uidb64)

    # GET USER FROM DATABASE BY ID
    try:
        user = UserManagement.objects.get(id=uid)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'User does not exist, wrong ID provided.'
        }

    #CHECK AND VERIFY TOKEN AND USER
   
    verify_token = check_token(user, token) 

    # If token is not verified, return error message
    if not verify_token:
        return {
                'status': 'error',
                'status_code': 409,
                'data': {},
                'error': 'Conflict',
                'message': 'Token mismatch, please generate new reset password link.'
            }
    
    # CHANGE USER PASSWORD AFTER TOKEN VERIFICATION
    if request.method == 'POST':

        data = json.loads(request.body)

        password = data['password']
        confirm_password = data['confirm_password']
        new_password = data['new_password']

        # VALIDATE PASSWORD FIELD
        try:

            # Check if confirm password match with password
            if confirm_password != password:
                raise ValidationError('Confirmation password must be equal to password.')

            # Check password length
            if len(password) < 8:
                if len(new_password) < 8:
                    raise ValidationError(f'The new password must be at least 8 characters')
                raise ValidationError(f'The password must be at least 8 characters')
            
            if len(password) > 16:
                if len(new_password) > 16:
                    raise ValidationError(f'The new password must be a maximum of 16 characters')
                raise ValidationError(f'The password must be a maximum of 16 characters')

        except Exception as e:
            return {
                'status': 'error',
                'status_code': 409,
                'data': {},
                'error': f'{e!r}',
                'message': e.message
            }
        
        # Get encoded password if user exists
        encoded = user.password

        # Check if password match and return user
        password = check_password(password, encoded)
        
        # if password check_returns false, raise error message
        if not password:
            return {
                'status': 'error',
                'status_code': 409,
                'data': {},
                'error': 'Conflict',
                'message': 'The old password is wrong, please provide right password.'
            }

        # Hash new password 
        new_password = make_password(new_password)

        # Update password field with new password
        user.password = new_password

        # Save new password into database
        user.save()

    # Create user dict with returned object values
    user: dict = {
        'first_name': user.first_name,
        'verify_token': verify_token
    }

    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'user': user
        },
        'error': None,
        'message': 'Ok',
    }
    