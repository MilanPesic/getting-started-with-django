import json

from django.contrib.auth.hashers import check_password
from django.core.exceptions import ValidationError
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt
from django.core.validators import validate_email
from django.contrib.auth import authenticate
from django.contrib.auth.models import update_last_login

from commons.decorators import required_fields
from commons.helpers import serialize
from user_api.models.user_management import UserManagement


@required_fields('email', 'password')
@csrf_exempt
@require_POST
def sign_in(request):
    """
    Exposed on route: 'api/1.0/users/sign-in'
    This route logs in user
    Expected payload:
        email,
        password
    """
    data = json.loads(request.body)

    email = data['email']
    password = data['password']

    # user = authenticate(username='john', password='secret')
    # if user is not None:
    # # A backend authenticated the credentials
    #     pass
    # else:
    # # No backend authenticated the credentials
    #     pass


    # VALIDATE PASSWORD AND EMAIL
    try:
        email_validation = validate_email(email)
        
        if len(password) < 8:
            raise ValidationError('This password must be at least 8 character long.')

        if len(password) > 16:
            raise ValidationError('This password must be at maximum 16 character long.')

    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': e.message
        }
    
    # FIND USER BY EMAIL
    try:
        user = UserManagement.objects.get(email=email)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'User not found, try different email address.'
        }


    # CHECK PASSWORD MATCH AND RETURN USER OBJECT
    try:
    
        # Get encoded password if user exists
        encoded = user.password

        # Check if password match and return user
        password = check_password(password, encoded)
        
        # Raise ValidationError if check_password returns false
        if not password:
            raise ValidationError('Password does not match encoded password.')

        # Last login from user 
        user.last_login = update_last_login(None, user)

        # Save last login into database
        user.save()

        # Create user dict with returned object values
        user: dict = {
            'first_name': user.first_name,
            'last_name': user.last_name
        }
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': e.message
        }
    
    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'user': user
        },
        'error': None,
        'message': 'Ok',
    }
