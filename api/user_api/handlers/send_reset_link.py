import json

from django.contrib.auth.hashers import check_password
from django.core.exceptions import ValidationError
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt
from django.core.validators import validate_email
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.template.loader import render_to_string

from commons.decorators import required_fields
from commons.helpers import serialize, make_token
from commons.mailer import send_email
from user_api.models.user_management import UserManagement


@require_POST
@csrf_exempt
@required_fields('email')
def send_reset_link(request):
    """
    Exposed on route: 'api/1.0/users/send_reset_link'
    This route allows sending link to user for resetting password 
    Expected payload:
        email
    """
    data = json.loads(request.body)

    email = data['email']
    attach_file = data.get('attach_file')

    # VALIDATE EMAIL
    try:
        email_validation = validate_email(email)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': e.message
        }
    
    # FIND USER BY EMAIL
    try:
        user = UserManagement.objects.get(email=email)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'User not found, try different email address.'
        }

    # CREATE RESET PASSWORD LINK FOR THE USER
    try:

        token = make_token(user)
        id = urlsafe_base64_encode(force_bytes(user.id))
        reset_link = f'https://api.dev.django/api/1.0/users/reset-password/{id}/{token}'
        reset_link_form = f'https://api.dev.django/api/1.0/users/reset-password-form/{id}/{token}'
        
        # Returned user object and serialize data values
        user = serialize(user)
        item_list = ['password', 'username', 'last_login', 'is_verified']
        user = {key: user[key] for key in user if key not in item_list}
        user['reset_link'] = reset_link
        user['reset_link_form'] = reset_link_form

    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': str(e)
        }

    # SEND LINK TO THE USER
    subject = 'RESET PASSWORD'
    from_email = 'admin@django.com'
    recipient_list = [email]

    context: dict = {
        'first_name': user['first_name'],
        'reset_link': reset_link
    }

    try:
        message = render_to_string('email/reset-password.txt', context)
        html_message = render_to_string('email/reset-password.html', context)
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': f'Template does not exist, check your path to the file.'
        }

    mail = send_email(
        subject,
        message,
        from_email,
        recipient_list,
        html_message,
        attach_file
    )

    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'user': user
        },
        'error': None,
        'message': 'Ok',
    }
