import json
import csv
import io

from xhtml2pdf import pisa
from weasyprint import HTML, CSS
from django.http import FileResponse, HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET, require_POST
from django.template.loader import render_to_string, get_template

from user_api.models import UserManagement
from commons.PDF import PDF


@require_POST
@csrf_exempt
def export_to_csv(request):
    """
    Exposed on route: '/api/1.0/users/export-to-csv'
    This route exports list of users to csv file
    Expected payload:
        /
    """
    # Set empty list of users so you can append data later
    list_of_users: list = []

    # GET ALL USERS FROM DATABASE 
    try:
        users = UserManagement.objects.all()
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'Something went wrong'
        }

    # LOOP THROUGH USERS AND EXTRACT DATA
    for user in users:
        list_of_users.append({
            'username': user.username,
            'email': user.email,
            'phone': user.phone,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'created_at': user.created_at,
        })

    # CHECK IF THERE ARE USERS SIGNED UP
    if not users:
        return {
            'status': 'success',
            'status_code': 200,
            'data': {},
            'error': None,
            'message': 'There are no signed up users.'
        }
    
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="users.csv"'},
    )

    # Counter for writing headers to csv file
    count = 0

    # Create the csv writer object
    writer = csv.writer(response)
    
    # Loop through list of profiles
    for user_data in list_of_users:
        if count == 0:

            # Write headers to csv file
            header = user_data.keys()
            writer.writerow(header)
            count += 1
        
        # Write data to csv file
        writer.writerow(user_data.values())
    
    return response


@require_POST
@csrf_exempt
def export_to_pdf_fpdf2(request):
    """
    Exposed on route: '/api/1.0/users/export-to-pdf-fpdf2'
    This route exports list of users to pdf file
    Expected payload:
        /
    """
    # Set empty list of users so you can append data later
    list_of_users: list = []

    # Get all users from database
    try:
        users = UserManagement.objects.all()
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'Something went wrong'
        }

    # Check if there are signed up users 
    if not users:
        return {
            'status': 'success',
            'status_code': 200,
            'data': {},
            'error': None,
            'message': 'There are no signed up users.'
        }

    # Render html template that should be converted to PDF
    template = render_to_string('users/list-of-users.html', {'users': users})
    
    # Create PDF instance object with default values
    pdf = PDF(orientation="L", unit="mm", format="A4")

    # There is no page for the moment, so we have to add one
    pdf.add_page()

    # Select font before printing
    pdf.set_font('helvetica', 'B', 16)

    # You can print text mixing different styles using HTML tags
    pdf.write_html(template)

    # Document is closed and saved, where output() returns the PDF bytearray buffer
    content = bytes(pdf.output())
    
    # Create the HttpResponse object with the appropriate PDF header.
    return HttpResponse(
        content=content, 
        content_type='application/pdf',
        headers={'Content-Disposition': 'attachment; filename="list-of-users.pdf"'},
    )


@require_POST
@csrf_exempt
def export_to_pdf_xhtml2pdf(request):
    """
    Exposed on route: '/api/1.0/users/export-to-pdf-xhtml2pdf'
    This route exports list of users to pdf file
    Expected payload:
        /
    """
    # Set empty list of users so you can append data later
    list_of_users: list = []

    # Get all users from database
    try:
        users = UserManagement.objects.all()
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'Something went wrong'
        }

    # Check if there are signed up users 
    if not users:
        return {
            'status': 'success',
            'status_code': 200,
            'data': {},
            'error': None,
            'message': 'There are no signed up users.'
        }

    # Render html template that should be converted to PDF
    template = render_to_string('users/list-of-users.html', {'users': users})

    # Create the HttpResponse object with the appropriate PDF header.
    response = HttpResponse(
        content_type='application/pdf',
        headers={'Content-Disposition': 'attachment; filename="list-of-users.pdf"'},
    )

    # Convert HTML to PDF
    status = pisa.CreatePDF(template, dest=response)

    # Return False on success and True on errors
    if status.err:
        return HttpResponse("The PDF could not be generated")

    return response 


@require_POST
@csrf_exempt
def export_to_pdf_weasyprint(request):
    """
    Exposed on route: '/api/1.0/users/export-to-pdf-weasyprint'
    This route exports list of users into pdf file
    Expected payload:
        /
    """
    # Set empty list of users so you can append data later
    list_of_users: list = []

    # Get all users from database
    try:
        users = UserManagement.objects.all()
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'Something went wrong'
        }

    # Check if there are signed up users 
    if not users:
        return {
            'status': 'success',
            'status_code': 200,
            'data': {},
            'error': None,
            'message': 'There are no signed up users.'
        }

    # Render html template that should be converted to PDF
    html_template = render_to_string('users/list-of-users.html', {'users': users})

    # Convert HTML to PDF
    html = HTML(string=html_template)

    # Create css template that should be applied to html template
    css_template = """
    body {
        color: green;
        }
    """
    
    # Create CSS object instance and get styles
    css = CSS(string=css_template)

    # Generate PDF file as bytes
    content = html.write_pdf(stylesheets=[css])

    return HttpResponse(
        content=content,
        content_type='application/pdf',
        headers={'Content-Disposition': 'attachment; filename="list-of-users.pdf"'},
    )
