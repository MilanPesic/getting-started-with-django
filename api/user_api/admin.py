from django.contrib import admin
from user_api.models import UserManagement


@admin.register(UserManagement)
class UserManagementAdmin(admin.ModelAdmin):
    list_display = ['email']
    list_filter = ['username']
    
    # def has_delete_permission(self, request, obj=None):
    #     # Disable delete
    #     return False

    # def has_add_permission(self, request, obj=None):
        # Disable save and add another 
    #     return False

    def has_change_permission(self, request, obj=None):
        # Disable change
        return False


    # def has_view_permission(self, request, obj=None):
    #     # Disable delete
    #     return False


    