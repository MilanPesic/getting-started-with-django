from django.db import models

from commons.validators import LengthValidator, FileValidator
from commons.validation import validate_file, validate_username_length, validate_password_length


class UserManagement(models.Model):
    username = models.CharField(blank=False, max_length=150, validators=[validate_username_length])
    password = models.CharField(blank=False, max_length=128, validators=[validate_password_length])
    email = models.EmailField(blank=False, max_length=254, unique=True, error_messages=({'unique': 'Email with this address already exists.'}))
    phone = models.CharField(blank=False, max_length=50)
    first_name = models.CharField(blank=False, max_length=150)
    last_name = models.CharField(blank=False, max_length=150)
    description = models.TextField(blank=False)
    is_active = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    last_login = models.DateTimeField(auto_now=True)
    upload_file = models.FileField(upload_to='user_api/uploads', null=True, blank=True, validators=[validate_file])


    # GET EMAIL FIELD NAME 
    def get_email_field_name(email):
        return str(email)
