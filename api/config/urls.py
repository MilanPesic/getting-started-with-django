"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.views import debug

from test_api_1.handlers import *
from user_api.handlers import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/1.0/profiles/all', profiles_all.profiles_all),
    path('api/1.0/profiles/get', profiles_get.profiles_get),
    path('api/1.0/profiles/create', profiles_create.profiles_create),
    path('api/1.0/profiles/update', profiles_update.profiles_update),
    path('api/1.0/profiles/delete', profiles_delete.profiles_delete),

    path('api/1.0/users/sign-up', sign_up.sign_up),
    path('api/1.0/users/sign-in', sign_in.sign_in),

    path('api/1.0/users/send-reset-link', send_reset_link.send_reset_link),
    path('api/1.0/users/reset-password/<uidb64>/<token>', reset_password.reset_password),
    path('api/1.0/users/reset-password-form/<uidb64>/<token>', reset_password_form.reset_password_form, name='reset-password-form'),

    path('api/1.0/users/send-verification-link', send_verification_link.send_verification_link),
    path('api/1.0/users/verify-user-account/<uidb64>/<token>', verify_user_account.verify_user_account),

    path('api/1.0/users/upload-file', upload_file.upload_file),

    path('api/1.0/users/export-to-csv', export_files.export_to_csv),
    path('api/1.0/users/export-to-pdf-fpdf2', export_files.export_to_pdf_fpdf2),
    path('api/1.0/users/export-to-pdf-xhtml2pdf', export_files.export_to_pdf_xhtml2pdf),
    path('api/1.0/users/export-to-pdf-weasyprint', export_files.export_to_pdf_weasyprint),

    path('api/1.0/users/standard-textual-search', search_result.standard_textual_search),
    path('api/1.0/users/full-text-search', search_result.full_text_search),

    path('', debug.default_urlconf),
]
