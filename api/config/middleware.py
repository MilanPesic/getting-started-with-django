from django.http import JsonResponse


# One-time configuration and initialization.
def json_response_middleware(get_response):
    
    # Return JSON response from handler
    def middleware(request):

        response = get_response(request)

        if isinstance(response, dict):
            response = JsonResponse(response)

        return response

    return middleware
