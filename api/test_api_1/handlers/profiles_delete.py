import json

from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from test_api_1.models.profiles import PersonProfile

@csrf_exempt
@require_POST
def profiles_delete(request):
    """
    Exposed on route: 'api/1.0/profiles/delete'
    This route deletes profile by requested id
    Expected payload:
        id
    """
    data = json.loads(request.body)
    id = data.get('id')
    
    # Delete profile from database by requested id
    try:
        profile = PersonProfile.objects.get(id=id)
        profile.delete()
        profile: dict = {
            'id': profile.id,
            'first_name': profile.first_name,
            'last_name': profile.last_name
        }
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'Error while deleting profile'
        }
    
    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'profile': profile
        },
        'error': None,
        'message': 'Ok',
    }
