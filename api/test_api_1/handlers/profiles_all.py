import json

from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from test_api_1.models.profiles import PersonProfile

@csrf_exempt
@require_POST
def profiles_all(request):
    """
    Exposed on route: 'api/1.0/profiles/all'
    This route returns all existing profiles
    Expected payload:
        first_name,
        last_name
    """
    data = json.loads(request.body)
    profiles: list = []
    
    # Return all profiles from database and serialize into json format
    profiles_list = serializers.serialize('json', PersonProfile.objects.all().order_by('id'))
    profiles_list = json.loads(profiles_list)

    for profile in profiles_list:
        profiles.append({
            'pk': profile['pk'],
            'profile': profile['fields']
        })

    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'profiles': profiles
        },
        'error': None,
        'message': 'Ok',
    }
