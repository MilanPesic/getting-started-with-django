import json

from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt

from test_api_1.models.profiles import PersonProfile

@csrf_exempt
@require_POST
def profiles_create(request):
    """
    Exposed on route: 'api/1.0/profiles/create'
    This route creates new profiles
    Expected payload:
        first_name,
        last_name
    """
    data = json.loads(request.body)
    
    first_name = data.get('first_name')
    last_name = data.get('last_name')

    # Create new profile for PersonProfile model
    try:
        profile = PersonProfile.objects.create(
                    first_name=first_name,
                    last_name=last_name
                )
        profile: dict = {
            'id': profile.id,
            'first_name': profile.first_name,
            'last_name': profile.last_name
        }
    except Exception as e:
        return {
            'status': 'error',
            'status_code': 409,
            'data': {},
            'error': f'{e!r}',
            'message': 'Error while creating profile'
        }
    
    return {
        'status': 'success',
        'status_code': 200,
        'data': {
            'profile': profile
        },
        'error': None,
        'message': 'Ok',
    }
