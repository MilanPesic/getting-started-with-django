import time

from django.core.management.base import BaseCommand
from django.db.utils import OperationalError


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Waiting for DB...")
        db = False
        while not db:
            try:
                self.check(databases=['default'])
                db = True
            except OperationalError:
                self.stdout.write("DB not READY...")
                time.sleep(1)

        self.stdout.write(self.style.SUCCESS('DB is OPERATIONAL...'))
        