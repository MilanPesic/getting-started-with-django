from django.contrib import admin
from test_api_1.models import PersonProfile


@admin.register(PersonProfile)
class PersonProfileAdmin(admin.ModelAdmin):
    pass
