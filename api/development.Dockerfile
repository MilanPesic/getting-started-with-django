FROM python:3.10.4-alpine3.15 as base

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apk add --no-cache build-base \
    linux-headers bsd-compat-headers \
    musl-dev openssl-dev libpq-dev postgresql-client \
    libffi-dev git curl rust cargo \
    gcc jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf \
    msttcorefonts-installer fontconfig && \
    update-ms-fonts && \
    fc-cache -f
RUN /usr/local/bin/python -m pip install --upgrade pip

FROM base AS builder
ENV PYTHONPATH=/usr/lib/python3.10/site-packages
RUN mkdir -p /build
WORKDIR /build
COPY /api/requirements.txt /build/requirements.txt
RUN pip install --prefix=/build -r /build/requirements.txt

FROM base
COPY --from=builder /build /usr/local
COPY /api /code
COPY /api/entrypoint.sh /entrypoint.sh
WORKDIR /code
RUN chmod +x /entrypoint.sh

CMD ["/entrypoint.sh"]
